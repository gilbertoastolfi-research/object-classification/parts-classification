from scipy.spatial import distance
import numpy as np
import os
import xml.etree.ElementTree as et
import cv2


class ImagePoint:
	def __init__(self, name_classes):
		self.name_classes = name_classes

	def readImage(self, image_path):
		image = cv2.imread(image_path, cv2.IMREAD_COLOR)
		return image

	def pathImageFromXml(self, xml_file):
		root = et.parse(xml_file).getroot()
		image_path = root.find("path").text
		return image_path

	def listBoundingBoxFromImage(self, xml_file):
		root = et.parse(xml_file).getroot()		
		bndbox = []
		for obj in root.findall('object'):
			class_name = obj.find('name').text
			xmin = int(obj.find('bndbox').find('xmin').text)
			ymin = int(obj.find('bndbox').find('ymin').text)
			xmax = int(obj.find('bndbox').find('xmax').text)
			ymax = int(obj.find('bndbox').find('ymax').text)
			bndbox.append([xmin,ymin,xmax,ymax,class_name])
		return bndbox

	def defineIdClass(self, cl_name):
		id_classe = -1
		for idx, name in self.name_classes.items():
			if name == cl_name:
				id_classe = idx
				break
		return id_classe

	#lê um xml com anotações e retorna uma lista de coordenadas juntamente com o id da classe.
	def xmlToPoint(self, xml_file):
		list_bndbox = self.listBoundingBoxFromImage(xml_file)
		image_points = []
		for bb in list_bndbox:
			x1 = bb[0]
			y1 = bb[1]
			x2 = bb[2]
			y2 = bb[3]
			rectangle_point = ((x1 + x2) / 2, (y1 + y2) / 2 )
			image_points.append([self.defineIdClass(bb[4]),rectangle_point])
		return image_points

	def drawPointImage(self, points, image):
		for point in points:
			p = (int(point[1][0]),int(point[1][1]))
			image = cv2.circle(image, p, radius=2, color=(0, 0, 255), thickness=2)
		return image

	def showImage(self, image):
		cv2.imshow('image',image)
		cv2.waitKey(0)

	#lê dicionário de predição da faster-rcnn e retorna uma lista de coordenadas juntamente com o id da classe.
	def predictionToPoints(self, dict_predition, path_image):
		image = self.readImage(path_image)
		detection_boxes = dict_predition['detection_boxes']
		detection_classes = dict_predition['detection_classes']
		height, width, color = image.shape
		image_points = []
		for box, id_class in zip(detection_boxes, detection_classes): #ymin, xmin, ymax, xmax
			y1 = box[0]
			x1 = box[1]
			y2 = box[2]
			x2 = box[3]
			x_min_abs = x1 * width
			x_max_abs = x2 * width
			y_min_abs = y1 * height
			y_max_abs = y2 * height			
			rectangle_point = ((x_min_abs + x_max_abs) / 2, (y_min_abs + y_max_abs) / 2 )
			image_points.append([id_class,rectangle_point])
		return image_points	

class Sequence:
	def __init__(self, filename, name_classes):
		self.filename = filename
		self.name_classes = name_classes
		path = os.path.dirname(self.filename)
		if not os.path.exists(path):
			os.makedirs(path)

	def listarXml(self, path):
		files_xml = []
		files = os.listdir(path)
		for file in files:
			name, extension = os.path.splitext(file)
			if extension.lower() == '.xml'.lower():
				if '_over_' not in name and '_aug_' not in name:
					xml = path + '/' + file
					files_xml.append(xml)
		return files_xml

	def criarSequenciaTest(self, output_dict, image_path):
		img_point = ImagePoint(self.name_classes)
		image_points = img_point.predictionToPoints(output_dict, image_path)
		sequencia = self.construirSequencia(image_points)
		classe_name = self.obterNomeClasse(image_path)
		self.salvarSequencia(classe_name, sequencia)

	def criarSequenciaTreinamento(self, path_conjunto):
		img_point = ImagePoint(self.name_classes)
		itens_conjunto = os.listdir(path_conjunto)
		for item in itens_conjunto:
			path = path_conjunto +'/'+item
			if os.path.isdir(path):
				files_xml = self.listarXml(path)
				for path_xml in files_xml:
					image_points = img_point.xmlToPoint(path_xml)
					sequencia = self.construirSequencia(image_points)
					classe_name = self.obterNomeClasse(path_xml)
					self.salvarSequencia(classe_name, sequencia)


	def prepararSequencia(self, sequencia, classe):
		list_para_string = ','.join(str(e) for e in sequencia)
		out = classe + ':[' + list_para_string + ']\n'
		return out

	def obterNomeClasse(self, image_path):
		filename = os.path.basename(image_path)
		classe_name = filename.split('_')[0]
		return classe_name


	def salvarSequencia(self, classe, sequencia):
		text = self.prepararSequencia(sequencia, classe)
		with open(self.filename, 'a') as file:
			file.write(text)

	# retorna o centróide de uma lista de pontos da imagem.
	def centroid(self, list_points):
		coodx = []
		coody = []
		for point in list_points:
			coodx.append(point[1][0])
			coody.append(point[1][1])
		x = sum(coodx)/len(coodx)
		y = sum(coody)/len(coody)
		return (x,y)

	def calcularPontoMaisProximo(self, image_points, centroide):
		points = []
		for imgp in image_points:
			points.append(imgp[1])
		disXY = distance.cdist(points, [centroide],'euclidean').min(axis=1)
		minimum = np.min(disXY)
		index_of_minimum = np.where(disXY == minimum)[0][0]
		return index_of_minimum

	#identifica o ponto mais próximo do centro da imagem e cria uma sequencia a partir dele.
	def construirSequencia(self, image_points):
		centroide = self.centroid(image_points)
		indice_proximo_centro = self.calcularPontoMaisProximo(image_points, centroide)
		sequencia = []
		sequencia.append(image_points[indice_proximo_centro][0])
		image_points.pop(indice_proximo_centro)
		while len(image_points) != 0:
			indice_proximo_centro = self.calcularPontoMaisProximo(image_points, centroide)
			sequencia.append(image_points[indice_proximo_centro][0])
			image_points.pop(indice_proximo_centro)
		return sequencia