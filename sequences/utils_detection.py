import numpy as np
import xml.etree.ElementTree as et
import json
import pickle
import os

class UtilsDetection:
	def __init__(self, file_pbtxt, file_classes_parts):
		self.file_pbtxt = file_pbtxt
		self.file_classes_parts = file_classes_parts
		self.tree = self.readFiles()

	def readFile(self, file_name):
		file = open(file_name, 'r')
		lines = file.readlines()
		idx = 0
		initial_idx = -1
		final_idx = -1
		classes = []
		for line in lines:	
			if '{' in line:
				initial_idx = idx
			if '}' in line:
				final_idx = idx		
			if initial_idx >= 0 and final_idx >= 0:
				block = ''.join(lines[initial_idx:final_idx+1])		
				initial_idx = block.index('{')
				final_idx = block.index('}')
				block = block[initial_idx:final_idx+1]
				classes.append(block)
				initial_idx = -1
				final_idx = -1		
			idx += 1
		return classes

	def estruturaFile(self, classes):
		item = {}
		for block in classes:
			bls = block.split('\n')
			variable = []
			for idx in range(1, len(bls)-1):			
				for pd in bls[idx].split(':'):
					s = pd.replace('"',"").replace(" ","")
					variable.append(s)
			item[variable[1]] = variable[3]
		return item

	def addPartsXML(self, tree, dict_classes):
		root = tree.getroot()	
		for name, part in dict_classes.items():
			for obj in root.findall('class'):
				name_obj = obj.find('name').text
				if name == name_obj:
					obj.find('parts').text = part
		return tree

	def createXML(self, dict_classes):
		xmlstring = '<classes>'
		for idx, name in dict_classes.items():
			xmlstring +='<class>'
			xmlstring +='<id>'+idx+'</id>'
			xmlstring +='<name>'+name+'</name>'
			xmlstring +='<parts>'+str(0)+'</parts>'
			xmlstring +='</class>'
		xmlstring+='</classes>'
		tree = et.ElementTree(et.fromstring(xmlstring))
		return tree

	def readFiles(self):
		classes = self.readFile(self.file_pbtxt)
		dict_classes = self.estruturaFile(classes)
		tree = self.createXML(dict_classes)
		classes = self.readFile(self.file_classes_parts)
		dict_classes = self.estruturaFile(classes)
		tree = self.addPartsXML(tree, dict_classes)
		return tree

	#lê o arquivo auxiliar, que possui o limite de classes em cada objeto (inseto), e retorna
	#um dicionario com a chave representando o id da classe e o valor o número de partes.
	def define_number_parts(self):
		classes = {}
		root = self.tree.getroot()	
		for obj in root.findall('class'):
			idx = obj.find('id').text
			parts = obj.find('parts').text
			classes[int(idx)] = int(parts)
		return classes

	def define_classes(self):
		classes = {}
		root = self.tree.getroot()	
		for obj in root.findall('class'):
			idx = obj.find('id').text
			name = obj.find('name').text
			classes[int(idx)] = name
		return classes

	#salva o dicionário de detecção, com uma determinada chave, no arquivo
	def salvarDeteccao(self, key, file_n, dict_to_file):
		if os.path.exists(file_n):
			dict_in_file = self.loadDict(file_n)
			dict_in_file[key] = dict_to_file
			self._saveDict(file_n, dict_in_file)
		else:
			dict_in_file = {}
			dict_in_file[key] = dict_to_file
			self._saveDict(file_n, dict_in_file)

	def _saveDict(self, file_name, output):
		with open(file_name,'wb') as pickle_out:
			pickle.dump(output,pickle_out)

	def loadDict(self, file_name):
		with open(file_name,'rb') as pickle_in:
			return pickle.load(pickle_in)

	# filtra o número máximo de partes por objeto (inseto)
	def filtrar_numero_deteccao(self, output, max_classes):
		out = {'num_detections': 0, 'detection_boxes': np.array([],dtype=np.float32),
					'detection_scores': np.array([],dtype=np.float32), 
					'detection_classes': np.array([], dtype=np.uint8)}
		detection_classes = output['detection_classes']
		idx = 0
		for dec in detection_classes:
			valor = max_classes[dec]
			if valor > 0:
				out['num_detections'] = out['num_detections'] + 1
				if len(out['detection_boxes']) == 0:
					out['detection_boxes'] = output['detection_boxes'][idx]
				else:
					out['detection_boxes'] = np.vstack((out['detection_boxes'], output['detection_boxes'][idx]))
				out['detection_scores'] = np.append(out['detection_scores'], output['detection_scores'][idx])
				out['detection_classes'] = np.append(out['detection_classes'], output['detection_classes'][idx])
				max_classes[dec] = valor - 1
			idx += 1
		return out

if __name__ == '__main__':
	
	
	output_dict = {'num_detections': 11, 'detection_boxes': np.array([ 
	[0.21415369, 0.6976416 , 0.39237443, 0.87627065],
	[0.17633913, 0.70232236, 0.3436966 , 0.8730936 ],
	[0.25122204, 0.6407913 , 0.47165075, 0.89657974],
	[0.56098884, 0.93334657, 0.64658326, 0.9881226 ],
	[0.24153659, 0.22259197, 0.4075186 , 0.403327  ],
	[0.2064418 , 0.00011303, 0.299861  , 0.05826257],
	[0.2054418 , 0.00211303, 0.299861  , 0.05826257],
	[0.2044418 , 0.00211303, 0.299861  , 0.05826257],
	[0.2034418 , 0.00211303, 0.299861  , 0.05826257],
	[0.1034418 , 0.00211303, 0.299861  , 0.05826257],
	[0.0934418 , 0.00211303, 0.299861  , 0.05826257]],dtype=np.float32),
	'detection_scores': np.array([0.13306457, 0.11320724, 0.10744191, 0.10428985, 
		0.09460889, 0.07478677, 0.06478677, 0.05478677, 0.04478677, 0.03478677, 0.02478677],dtype=np.float32), 
	'detection_classes': np.array([2, 8, 3, 8, 1, 4,8,4,8,1,3], dtype=np.uint8)}


	file_pbtxt = './content/repetition/repetition1/train/repetition1_train.pbtxt'
	file_classes_parts = './content/classes_parts.txt'

	utils_detect = UtilsDetection(file_pbtxt, file_classes_parts)
	key = '333'
	file_pkl = 'repetition1_train.pkl'
	utils_detect.salvarDeteccao(key, file_pkl, output_dict)
	print(utils_detect.loadDict(file_pkl))
	'''
	number_parts = utils_detect.define_number_parts()
	print(number_parts)
	name_classes = utils_detect.define_classes()
	print(name_classes)
	print(utils_detect.filtrar_numero_deteccao(output_dict,number_parts))
	'''