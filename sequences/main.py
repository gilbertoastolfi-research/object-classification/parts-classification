import numpy as np
import cv2
from utils_detection import UtilsDetection
#from utils_image_points import ImagePoint
from sequence import Sequence

if __name__ == '__main__':
	#Abaixo exemplo para obter sequencia de treinamento
	'''
	path_conjunto = './content/repetition/repetition1/train'
	file_pbtxt = path_conjunto + '/repetition1_train.pbtxt'
	file_classes_parts = './content/classes_parts.txt'

	utils_detect = UtilsDetection(file_pbtxt, file_classes_parts)
	name_classes = utils_detect.define_classes()
	file_name = path_conjunto+'/repetition1_train_sequence.txt'
	seq = Sequence(file_name,name_classes)
	seq.criarSequenciaTreinamento(path_conjunto)
	'''
	

	#Abaixo exemplo para obter sequencia de treinamento
	
	path_conjunto = './content/repetition/repetition1/test'
	file_pbtxt = './content/repetition/repetition1/train/repetition1_train.pbtxt'
	file_classes_parts = './content/classes_parts.txt'

	utils_detect = UtilsDetection(file_pbtxt, file_classes_parts)
	name_classes = utils_detect.define_classes()
	file_name = path_conjunto+'/repetition1_test_sequence.txt'
	seq = Sequence(file_name,name_classes)


	#saída da Faster-rcnn
	output_dict = {'num_detections': 11, 'detection_boxes': np.array([ 
	[0.21415369, 0.6976416 , 0.39237443, 0.87627065],
	[0.17633913, 0.70232236, 0.3436966 , 0.8730936 ],
	[0.25122204, 0.6407913 , 0.47165075, 0.89657974],
	[0.56098884, 0.93334657, 0.64658326, 0.9881226 ],
	[0.24153659, 0.22259197, 0.4075186 , 0.403327  ],
	[0.2064418 , 0.00011303, 0.299861  , 0.05826257],
	[0.2054418 , 0.00211303, 0.299861  , 0.05826257],
	[0.2044418 , 0.00211303, 0.299861  , 0.05826257],
	[0.2034418 , 0.00211303, 0.299861  , 0.05826257],
	[0.1034418 , 0.00211303, 0.299861  , 0.05826257],
	[0.0934418 , 0.00211303, 0.299861  , 0.05826257]],dtype=np.float32),
	'detection_scores': np.array([0.13306457, 0.11320724, 0.10744191, 0.10428985, 
		0.09460889, 0.07478677, 0.06478677, 0.05478677, 0.04478677, 0.03478677, 0.02478677],dtype=np.float32), 
	'detection_classes': np.array([2, 8, 3, 8, 1, 4,8,4,8,1,3], dtype=np.uint8)}

	path_image = './content/repetition/repetition1/test/nezara/nezara_2.jpg'
	seq.criarSequenciaTest(output_dict, path_image)	

	'''
	image_path = img_point.pathImageFromXml(path_xml)
	image = img_point.readImage(image_path)
	image = img_point.drawPointImage(image_points, image)
	p = (int(controide[0]),int(controide[1]))
	image = cv2.circle(image, p, radius=2, color=(255, 0, 0), thickness=2)
	img_point.showImage(image)
	'''
	